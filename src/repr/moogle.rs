extern crate uuid;
use uuid::Uuid;

use crate::repr::MogId;

#[derive(Debug, Eq, PartialEq)]
pub enum MogBusy {
    Working,
    Relaxing,
    Sleeping
}

#[derive(Debug, Eq, PartialEq)]
pub struct Moogle {
    is_busy: MogBusy,
    name: String,
    id: MogId,
    needs: MoggleMood
}

#[derive(Debug, Eq, PartialEq)]
pub struct MoggleMood {
    hunger: u16,
    thirst: u16,
    sleepiness: u16,
    boredom: u16
}

impl Moogle {
    pub fn new(name: String) -> Moogle {
        Moogle {
            is_busy: MogBusy::Relaxing,
            name,
            id: Uuid::new_v4(),
            needs: MoggleMood {
                hunger: 0,
                thirst: 0,
                sleepiness: 0,
                boredom: 0
            }
        }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }

    pub fn get_id(&self) -> MogId {
        self.id
    }
}
