use uuid::Uuid;

pub type MogId = Uuid;
pub type TaskId = Uuid;

pub mod job;
pub mod moogle;
pub mod workstation;
