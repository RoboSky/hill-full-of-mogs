extern crate uuid;

use std::collections::VecDeque;

use uuid::Uuid;

use crate::repr::{MogId, TaskId};
use crate::repr::moogle::Moogle;
use crate::repr::workstation::Workstation;

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum ContingencyPlan {
    Wait,
    Postpone,
    Cancel
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum Status {
    Running,
    Stopped,
    Scheduled
}

enum Details {}

pub struct Task<'a> {
    assignee: &'a Moogle,
    station: Option<&'a Workstation>,
    id: Uuid,
    details: Details,
    status: Status
}

pub struct TaskQueue<'a> {
    tasks: VecDeque<Task<'a>>
}

impl<'a> TaskQueue<'a> {
    pub fn postpone(&mut self, task_id: TaskId) {
        let position = self
            .tasks
            .iter()
            .position(|x| x.id == task_id)
            .expect(&format!(
                "[Postpone] task_id not found: {}",
                task_id.to_hyphenated()
            ));
        let task = self.tasks.remove(position).unwrap();
        self.tasks.push_back(task);
    }

    pub fn assign(&mut self, assignee: &'a Moogle, station: &'a Workstation, details: Details) {
        let id = Uuid::new_v4();
        self.tasks.push_back(Task {
            assignee,
            station: Some(station),
            id: Uuid::new_v4(),
            details,
            status: Status::Scheduled
        });
    }
}
