pub enum Material {
    Air
}

impl Material {
    pub fn is_traversable(&self) -> bool {
        match self {
            Material::Air => true
        }
    }
    pub fn supports(&self) -> bool {
        match self {
            Material::Air => false
        }
    }
}
