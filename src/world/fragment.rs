use std::mem::{self, MaybeUninit};
use std::ops::{Index, IndexMut};

use crate::world::tile::Point;

pub struct Neighborhood {
    cells: Box<[[[Point; 128]; 256]; 256]>
}

impl Index<(u32, u32, u32)> for Neighborhood {
    type Output = Point;

    fn index(&self, index: (u32, u32, u32)) -> &Self::Output {
        let x: &[[Point; 128]; 256] = &self.cells[index.0 as usize];
        let y: &[Point; 128] = &x[index.1 as usize];
        &y[index.2 as usize]
    }
}

impl IndexMut<(u32, u32, u32)> for Neighborhood {
    fn index_mut(&mut self, index: (u32, u32, u32)) -> &mut Self::Output {
        let x: &mut [[Point; 128]; 256] = &mut self.cells[index.0 as usize];
        let y: &mut [Point; 128] = &mut x[index.1 as usize];
        &mut y[index.2 as usize]
    }
}
