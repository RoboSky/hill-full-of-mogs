use std::convert::TryInto;
use std::slice;

use crate::repr::job::{Task, TaskQueue};
use crate::repr::moogle::Moogle;
use std::path::PathBuf;

pub mod entity;
pub mod fragment;
pub mod tile;

pub type Seed = u64;

pub struct World<'a> {
    name: String,
    moogles: Vec<Moogle>,
    tasks: TaskQueue<'a>,
    save_location: PathBuf,
    world_seed: Seed
}

impl<'a> World<'a> {}
