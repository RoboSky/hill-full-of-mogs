use crate::material::Material;
use crate::world::entity::Mob;
use std::collections::HashSet;

pub struct Point {
    fill: Material,
    floor: Material,
    mobs: HashSet<Mob>,
    items: HashSet<PointItem>,
    light_level: u8
}

impl Point {
    pub fn empty() -> Point {
        Point {
            fill: Material::Air,
            floor: Material::Air,
            mobs: HashSet::new(),
            items: HashSet::new(),
            light_level: 0
        }
    }

    pub fn is_traversable_h(&self) -> bool {
        self.fill.is_traversable()
    }
    pub fn is_traversable_v(&self) -> bool {
        self.fill.is_traversable() && self.floor.is_traversable()
    }

    pub fn supports(&self) -> bool {
        self.floor.supports()
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum PointItem {}
