use crate::repr::MogId;
use crate::repr::moogle::Moogle;

#[derive(Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum Mob {
    MovingMoogle(MogId)
}
