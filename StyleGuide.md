General
=======
* In general, where we do not contradict it, we defer to [this style guide](https://github.com/rust-dev-tools/fmt-rfcs/blob/master/guide/guide.md).
* All rules have exceptions.

Formatting
==========
Lists of things
---------------
* Trailing commas are to be avoided. For example, `(long, list, of, arguments)` and not `(long, list, of, arguments,)`.
* Lists (function arguments, struct initialization, array items, etc.) should be arranged either horizontally or vertically, and not a mix of the two.
```rust
// Okay
foo(bar, baz, qaz, quux);
foo(bar,
    baz,
    qaz,
    quux);
// Not okay
foo(bar, baz,
    qaz, quux);
foo(bar,
    baz,
    qaz, quux);
```
* Lists should be arranged vertically if any of their items contain nontrivial calculations (for either the computer or the human to process).
* In vertically-arranged lists, the opening delimiter and first item should written inline, with all subsequent items being aligned with the start of the first item. The closing delimiter should be inline with the last item.
```rust
// Okay
foo(lorem,
    ipsum,
    dolor,
    sit,
    amet);
// Not okay
foo(
    lorem,
    ipsum,
    dolor,
    sit,
);
```
This extends to long chains of operators as well, e.g.,
```rust
let sum = foo
        + baz
        + qaz;
```
* Lines should be no longer than 120 characters. If a line is only a little longer than 120 characters, consider splitting it up with newlines. If said line is difficult to split up with newlines, don't worry about it.
* Comment lines may be as long as desired, as long as the line is no longer than 120 characters. Even if a comment is not over the character limit, consider using line breaks to improve readability.

Naming conventions
==================
* Functions returning `bool` (or a similar type, such as `Result<bool, _>`) should generally begin with `is_`. Variables and struct fields with type `bool` should also begin with `is_`, unless their name makes their type completely clear, e.g. `acceptable`, `saveable`, etc.
* Type annotations do not belong in variable names, e.g. `let x_usize = y.len()`. If a type is not obvious, include its type annotation (`let x: usize = y.len()`).
* The adjective form of "Moogle" is "Mog".
